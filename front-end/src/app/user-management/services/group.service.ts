import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Group } from '../model/group';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  constructor(private httpClient: HttpClient) {}

  private url = `${environment.backendUrl}/groups`;

  creatStudentGroup(group: Group): Observable<void> {
    return this.httpClient.post<void>(this.url, group);
  }

  getGroups(
    sort: string,
    order: SortDirection,
    page: number
  ): Observable<any> {
    const requestUrl = `${this.url}?sort=${sort}&order=${order}&page=${page}`;

    return this.httpClient.get<any>(requestUrl);
  }
}
