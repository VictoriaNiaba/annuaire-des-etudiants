import { Student } from './student';

export interface Group {
  name: string;
  students: Student[];
}
