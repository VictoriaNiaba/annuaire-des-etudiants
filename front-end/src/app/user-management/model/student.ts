export interface Student {
  linkedin?: string;
  dateOfBirth: Date;
  userAccount: {
    firstname: string;
    lastname: string;
    emailAddress: string;
  };
}
