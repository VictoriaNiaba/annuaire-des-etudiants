import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { GroupCreateFormComponent } from './components/group-create-form/group-create-form.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { UserManagementPageComponent } from './components/user-management-page/user-management-page.component';
import { UserManagementRoutingModule } from './user-management-routing.module';

@NgModule({
  declarations: [
    UserManagementPageComponent,
    GroupListComponent,
    GroupCreateFormComponent,
  ],
  imports: [SharedModule, UserManagementRoutingModule, HttpClientModule],
})
export class UserManagementModule {}
