export interface InformationSheet {
  identity: Identity;
  group: Group;
  sections: Section[];
}

export interface Group {
  id: number;
  name: string;
}

export interface Identity {
  firstname: string;
  lastname: string;
  emailAddress: string;
  linkedin: string;
  dateOfBirth: string;
}

export interface Section {
  title: string;
  fields: Field[];
}

export interface Field {
  questionId: number;
  question: string;
  type: string;
  answer: null | string;
}
