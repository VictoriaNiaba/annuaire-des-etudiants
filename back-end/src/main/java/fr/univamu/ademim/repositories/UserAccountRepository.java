package fr.univamu.ademim.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    UserAccount findByEmailAddress(String emailAddress);
}
