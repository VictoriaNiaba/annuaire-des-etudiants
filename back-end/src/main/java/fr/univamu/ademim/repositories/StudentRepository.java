package fr.univamu.ademim.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findByUserAccountId(Long id);

    Optional<Student> findByUserAccountEmailAddress(String emailAddress);

}