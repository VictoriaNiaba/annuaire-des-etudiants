package fr.univamu.ademim.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.Role;
import fr.univamu.ademim.model.Role.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName name);
}
