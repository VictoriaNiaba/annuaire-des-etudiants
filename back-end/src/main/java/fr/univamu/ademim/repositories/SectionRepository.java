package fr.univamu.ademim.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.Section;

public interface SectionRepository extends JpaRepository<Section, Long> {
    Optional<Section> findByTitle(String title);
}