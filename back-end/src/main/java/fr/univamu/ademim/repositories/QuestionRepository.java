package fr.univamu.ademim.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
    Optional<Question> findByLabel(String label);
}