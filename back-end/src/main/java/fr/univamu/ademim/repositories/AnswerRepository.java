package fr.univamu.ademim.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univamu.ademim.model.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {

}