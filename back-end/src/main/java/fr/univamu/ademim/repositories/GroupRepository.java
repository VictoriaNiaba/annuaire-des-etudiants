package fr.univamu.ademim.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import fr.univamu.ademim.model.Group;

@Repository
@RepositoryRestResource
public interface GroupRepository extends JpaRepository<Group, Long> {
    @Override
    @RestResource
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    Page<Group> findAll(Pageable pageable);

    @Override
    @RestResource
    <S extends Group> S save(S group);
}
