package fr.univamu.ademim.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable, Comparable<Role> {

    private static final long serialVersionUID = 1L;
    private static final String ROLE_PREFIX = "ROLE_";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private RoleName name;

    public Role(RoleName name) {
        this.name = name;
    }

    /**
     * Returns the {@link Role}'s name as Spring Security authority token so that it
     * can be used in {@code hasRole('…')} expressions.
     *
     * @return
     */
    public String toAuthority() {
        return name.toString().startsWith(ROLE_PREFIX)
                ? name.toString()
                : ROLE_PREFIX.concat(name.toString());
    }

    @Override
    public String toString() {
        return name.toString();
    }

    @Override
    public final int compareTo(Role other) {
        return this.name.compareTo(other.name);
    }

    public static enum RoleName {
        ADMINISTRATOR,
        TEACHER,
        STUDENT
    }

}
