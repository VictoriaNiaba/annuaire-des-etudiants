package fr.univamu.ademim.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 255, nullable = true, unique = true)
	private String linkedin;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(length = 255, nullable = false)
	private LocalDate dateOfBirth;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private Group group;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_account_id", referencedColumnName = "id")
	private UserAccount userAccount;

	@OneToMany(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", referencedColumnName = "id")
	private List<Answer> answers = new ArrayList<>();


}
