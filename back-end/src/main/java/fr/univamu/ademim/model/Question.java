package fr.univamu.ademim.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 255, nullable = false)
    private String label;

    @Column(length = 255, nullable = false)
    @Enumerated(EnumType.STRING)
    private QuestionType type;

    @Column(length = 255, nullable = true)
    private String placeholder;

    @Column(length = 255, nullable = true)
    private String hint;

    @Column(nullable = false)
    private boolean required;

    public static enum QuestionType {
        SELECT,
        INPUT_TEXT,
        TEXT_AREA
    }

}
