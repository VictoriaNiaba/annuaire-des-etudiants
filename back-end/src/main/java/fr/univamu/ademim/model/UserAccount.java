package fr.univamu.ademim.model;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class UserAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 255, nullable = false)
	private String firstname;

	@Column(length = 255, nullable = false)
	private String lastname;

	@Column(length = 255, nullable = false, unique = true)
	private String emailAddress;

	@JsonIgnore
	@Column(length = 100, nullable = false)
	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Role> roles = new TreeSet<Role>();

	public boolean addRole(Role role) {
		return roles.add(role);
	}

	public boolean removeRole(Role role) {
		return roles.remove(role);
	}

	public boolean hasRole(Role role) {
		return roles.contains(role);
	}

}
