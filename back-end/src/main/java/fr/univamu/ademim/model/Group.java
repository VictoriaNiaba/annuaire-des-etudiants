package fr.univamu.ademim.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "student_group")
@Getter
@Setter
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 255, nullable = false, unique = true)
	private String name;

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
	private List<Student> students = new ArrayList<>();

	@ManyToMany
	@JoinTable(name = "Groups_sections", //
			joinColumns = @JoinColumn(name = "group_id"), //
			inverseJoinColumns = @JoinColumn(name = "section_id"))
	private List<Section> sections = new ArrayList<>();

	public void setStudents(List<Student> students) {
		this.students = students;

		for (Student student : students) {
			student.setGroup(this);
		}
	}
}