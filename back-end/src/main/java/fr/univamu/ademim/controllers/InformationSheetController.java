package fr.univamu.ademim.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univamu.ademim.dto.InformationSheetDto;
import fr.univamu.ademim.services.InformationSheetService;

@RestController
@RequestMapping(produces = "application/json")
public class InformationSheetController {

    @Autowired
    private InformationSheetService informationSheetService;

    @GetMapping("students/myself/information-sheet")
    @PreAuthorize("hasRole('STUDENT')")
    public ResponseEntity<InformationSheetDto> findMyInformationSheet() {
        InformationSheetDto informationSheetDto //
                = informationSheetService.findMyInformationSheet();

        return ResponseEntity.ok(informationSheetDto);
    }
}
