package fr.univamu.ademim.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.services.UserAccountService;

@RestController
public class AuthController {

	@Autowired
	private UserAccountService userAccountService;

	/**
	 * Cette route permet aussi bien de se connecter que de récupérer l'utilisateur
	 * courant.
	 */
	@GetMapping("/user")
	public UserAccount user() {
		return userAccountService.getCurrentUserAccount();
	}
}
