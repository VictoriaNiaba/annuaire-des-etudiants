package fr.univamu.ademim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdemimApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdemimApplication.class, args);
	}

}
