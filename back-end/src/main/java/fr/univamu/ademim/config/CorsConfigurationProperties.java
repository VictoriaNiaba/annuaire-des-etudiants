package fr.univamu.ademim.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "application.cors")
public class CorsConfigurationProperties {

    private List<String> allowedOrigins;
    private List<String> allowedHeaders;
    private List<String> allowedMethods;

}
