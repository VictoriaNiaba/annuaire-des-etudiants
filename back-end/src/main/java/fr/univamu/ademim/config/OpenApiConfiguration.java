package fr.univamu.ademim.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class OpenApiConfiguration {

    @Bean
    public OpenAPI customOpenAPI() {

        Info info = new Info().title("ADEMIM").version("1.0.0").description("");
        return new OpenAPI().info(info);
    }
}