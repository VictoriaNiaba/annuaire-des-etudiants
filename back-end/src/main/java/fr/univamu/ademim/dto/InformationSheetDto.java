package fr.univamu.ademim.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class InformationSheetDto {

    private IdentityDto identity;

    private GroupDto group;

    private List<SectionDto> sections;

    @Data
    public static class IdentityDto {

        private String firstname;

        private String lastname;

        private String emailAddress;

        private String linkedin;

        private LocalDate dateOfBirth;

    }

    @Data
    public static class GroupDto {

        private long id;

        private String name;

    }

    @Data
    public static class SectionDto {

        private String title;

        private List<FieldDto> fields = new ArrayList<>();

        @Data
        public static class FieldDto {

            private long questionId;

            private String question;

            private String type;

            private String answer;

        }
    }
}
