package fr.univamu.ademim.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.repositories.UserAccountRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Override
    public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {
        UserAccount userAccount = userAccountRepository.findByEmailAddress(emailAddress);
        if (userAccount == null) {
            System.out.println("user not found");
            throw new UsernameNotFoundException(emailAddress);
        }
        System.out.println(userAccount.getEmailAddress());
        return new UserDetailsImpl(userAccount);
    }
}