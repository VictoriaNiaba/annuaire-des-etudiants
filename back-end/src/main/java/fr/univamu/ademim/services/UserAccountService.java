package fr.univamu.ademim.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.repositories.UserAccountRepository;

@Service
public class UserAccountService {
    @Autowired
    private UserAccountRepository userAccountRepository;

    public UserAccount getCurrentUserAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserAccountEmailAddress = authentication.getName();
        return userAccountRepository.findByEmailAddress(currentUserAccountEmailAddress);
    }
}
