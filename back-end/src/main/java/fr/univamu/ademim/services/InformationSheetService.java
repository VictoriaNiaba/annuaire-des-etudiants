package fr.univamu.ademim.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univamu.ademim.dto.InformationSheetDto;
import fr.univamu.ademim.dto.InformationSheetDto.GroupDto;
import fr.univamu.ademim.dto.InformationSheetDto.IdentityDto;
import fr.univamu.ademim.dto.InformationSheetDto.SectionDto;
import fr.univamu.ademim.dto.InformationSheetDto.SectionDto.FieldDto;
import fr.univamu.ademim.model.Answer;
import fr.univamu.ademim.model.Group;
import fr.univamu.ademim.model.Question;
import fr.univamu.ademim.model.Section;
import fr.univamu.ademim.model.Student;
import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.repositories.StudentRepository;

@Service
public class InformationSheetService {

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private StudentRepository studentRepository;

    public InformationSheetDto findMyInformationSheet() {
        Student currentStudent = getCurrentStudent();

        IdentityDto identity = buildIdentity(currentStudent);
        GroupDto group = buildGroup(currentStudent);
        List<SectionDto> sections = buildSections(currentStudent);

        InformationSheetDto informationSheet = new InformationSheetDto();
        informationSheet.setIdentity(identity);
        informationSheet.setGroup(group);
        informationSheet.setSections(sections);

        return informationSheet;
    }

    private Student getCurrentStudent() {
        UserAccount currentUserAccount = userAccountService.getCurrentUserAccount();

        return studentRepository
                .findByUserAccountId(currentUserAccount.getId())
                .orElseThrow(() -> new RuntimeException("The current authenticated user is not a student"));
    }

    private IdentityDto buildIdentity(Student currentStudent) {
        IdentityDto identity = new IdentityDto();
        UserAccount currentUserAccount = currentStudent.getUserAccount();

        identity.setFirstname(currentUserAccount.getFirstname());
        identity.setLastname(currentUserAccount.getLastname());
        identity.setEmailAddress(currentUserAccount.getEmailAddress());
        identity.setDateOfBirth(currentStudent.getDateOfBirth());
        identity.setLinkedin(currentStudent.getLinkedin());

        return identity;
    }

    private GroupDto buildGroup(Student currentStudent) {
        GroupDto groupDto = new GroupDto();
        Group group = currentStudent.getGroup();

        groupDto.setId(group.getId());
        groupDto.setName(group.getName());

        return groupDto;
    }

    private List<SectionDto> buildSections(Student currentStudent) {

        List<Section> currentGroupSections = currentStudent.getGroup().getSections();
        List<SectionDto> sectionDtoList = new ArrayList<>();
        Map<Long, Answer> answersByQuestionId = getAnswersByQuestionId(currentStudent);

        for (Section section : currentGroupSections) {
            SectionDto sectionDto = new SectionDto();
            sectionDto.setTitle(section.getTitle());

            List<FieldDto> fieldDtoList = new ArrayList<>();
            List<Question> questions = section.getQuestions();
            for (Question question : questions) {
                Answer answer = answersByQuestionId.get(question.getId());
                FieldDto fieldDto = buildFieldDto(question, answer);

                fieldDtoList.add(fieldDto);
            }
            sectionDto.setFields(fieldDtoList);

            sectionDtoList.add(sectionDto);
        }

        return sectionDtoList;
    }

    private Map<Long, Answer> getAnswersByQuestionId(Student currentStudent) {
        return currentStudent.getAnswers().stream()
                .collect(Collectors.toMap(answer -> answer.getQuestion().getId(), Function.identity()));
    }

    private FieldDto buildFieldDto(Question question, Answer answer) {
        FieldDto fieldDto = new FieldDto();

        fieldDto.setQuestionId(question.getId());
        fieldDto.setQuestion(question.getLabel());
        if (answer != null) {
            fieldDto.setAnswer(answer.getValue());
        }
        fieldDto.setType(question.getType().toString());

        return fieldDto;
    }
}