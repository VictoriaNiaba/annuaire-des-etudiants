package fr.univamu.ademim.datainitializers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
class DataInitializerInvoker implements ApplicationRunner {

    @Autowired
    private List<DataInitializer> initializers;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (DataInitializer initializer : initializers) {
            initializer.initialize();
        }
    }
}