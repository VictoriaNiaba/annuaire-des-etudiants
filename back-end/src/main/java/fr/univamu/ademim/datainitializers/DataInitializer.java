package fr.univamu.ademim.datainitializers;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public interface DataInitializer {

    @Transactional
    void initialize();
}