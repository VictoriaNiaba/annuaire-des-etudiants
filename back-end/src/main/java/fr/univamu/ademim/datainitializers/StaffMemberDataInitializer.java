package fr.univamu.ademim.datainitializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.exception.RoleNotFoundException;
import fr.univamu.ademim.model.Role;
import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.model.Role.RoleName;
import fr.univamu.ademim.repositories.RoleRepository;
import fr.univamu.ademim.repositories.UserAccountRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(2)
@Component
public class StaffMemberDataInitializer implements DataInitializer {

    private static final String ROLE_NOT_FOUND = "Role %s was not found";
    private static final String DEFAULT_PASSWORD = "pwd";

    @Autowired
    private UserAccountRepository userAccountRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void initialize() {
        if (userAccountRepository.count() > 0) {
            return;
        }

        log.info("Creating default staff members.");

        initializeDefaultAdministrator();
        initializeDefaultTeacher();
    }

    private void initializeDefaultAdministrator() {
        Role adminRole = roleRepository.findByName(RoleName.ADMINISTRATOR)
                .orElseThrow(() -> new RoleNotFoundException(String.format(ROLE_NOT_FOUND, RoleName.ADMINISTRATOR)));

        UserAccount adminAccount = new UserAccount();
        adminAccount.addRole(adminRole);
        adminAccount.setEmailAddress("sionne.niaba@gmail.com");
        adminAccount.setFirstname("Victoria");
        adminAccount.setLastname("Niaba");
        adminAccount.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
        userAccountRepository.save(adminAccount);
    }

    private void initializeDefaultTeacher() {
        Role teacherRole = roleRepository.findByName(RoleName.TEACHER)
                .orElseThrow(() -> new RoleNotFoundException(String.format(ROLE_NOT_FOUND, RoleName.TEACHER)));

        UserAccount teacherAccount = new UserAccount();
        teacherAccount.addRole(teacherRole);
        teacherAccount.setEmailAddress("claude.delorme@gmail.com");
        teacherAccount.setFirstname("Claude");
        teacherAccount.setLastname("Delorme");
        teacherAccount.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
        userAccountRepository.save(teacherAccount);
    }

}