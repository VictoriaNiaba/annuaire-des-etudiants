package fr.univamu.ademim.datainitializers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.model.Question;
import fr.univamu.ademim.model.Section;
import fr.univamu.ademim.model.Question.QuestionType;
import fr.univamu.ademim.repositories.SectionRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(3)
@Component
public class SectionDataInitializer implements DataInitializer {

    @Autowired
    private SectionRepository sectionRepository;

    @Override
    public void initialize() {
        if (sectionRepository.count() > 0) {
            return;
        }

        log.info("Creating default sections.");

        initializeResearchSection();
    }

    private void initializeResearchSection() {
        Section research = new Section();
        research.setTitle("Recherche");

        Question situation = new Question();
        situation.setLabel("Votre situation");
        situation.setType(QuestionType.SELECT);
        situation.setRequired(true);

        Question subjectOfYourDoctorate = new Question();
        subjectOfYourDoctorate.setLabel("Sujet de votre doctorat");
        subjectOfYourDoctorate.setType(QuestionType.TEXT_AREA);
        subjectOfYourDoctorate.setRequired(true);

        Question hostLaboratoryName = new Question();
        hostLaboratoryName.setLabel("Nom du laboratoire d’accueil");
        hostLaboratoryName.setType(QuestionType.INPUT_TEXT);
        hostLaboratoryName.setRequired(true);

        Question comments = new Question();
        comments.setLabel("Commentaires libres");
        comments.setType(QuestionType.TEXT_AREA);
        comments.setRequired(false);

        research.setQuestions(List.of( //
                situation, //
                subjectOfYourDoctorate, //
                hostLaboratoryName, //
                comments //
        ));

        sectionRepository.save(research);
    }

}