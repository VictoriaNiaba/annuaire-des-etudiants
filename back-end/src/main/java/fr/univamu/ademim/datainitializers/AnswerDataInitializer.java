package fr.univamu.ademim.datainitializers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.model.Answer;
import fr.univamu.ademim.model.Student;
import fr.univamu.ademim.repositories.AnswerRepository;
import fr.univamu.ademim.repositories.QuestionRepository;
import fr.univamu.ademim.repositories.StudentRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(5)
@Component
public class AnswerDataInitializer implements DataInitializer {

        @Autowired
        private AnswerRepository answerRepository;
        @Autowired
        private StudentRepository studentRepository;
        @Autowired
        private QuestionRepository questionRepository;

        @Override
        public void initialize() {
                if (answerRepository.count() > 0) {
                        return;
                }

                log.info("Creating default answers.");

                Student student = studentRepository.findByUserAccountEmailAddress("lina.caxeiro@gmail.com")
                                .orElseThrow(() -> new RuntimeException(
                                                "Student with email address 'lina.caxeiro@gmail.com' was not found"));

                Answer situationAnswer = new Answer();
                situationAnswer.setValue("Je suis en 1ère année");
                situationAnswer.setQuestion(questionRepository.findByLabel("Votre situation")
                                .orElseThrow(() -> new RuntimeException("Question 'Situation' was not found.")));

                Answer subjectOfYourDoctorateAnswer = new Answer();
                subjectOfYourDoctorateAnswer.setValue(
                                "Une Feuille de Route Basée sur des Evidences pour Soutenir L'ingenierie de Logiciels pour L'internet Des Objets");
                subjectOfYourDoctorateAnswer.setQuestion(questionRepository.findByLabel("Sujet de votre doctorat")
                                .orElseThrow(() -> new RuntimeException(
                                                "Question 'Sujet de votre doctorat' was not found.")));

                Answer hostLaboratoryNameAnswer = new Answer();
                hostLaboratoryNameAnswer.setValue(
                                "Laboratoire des Sciences de l'information et des Systèmes (LSIS)");
                hostLaboratoryNameAnswer.setQuestion(questionRepository.findByLabel("Nom du laboratoire d’accueil")
                                .orElseThrow(() -> new RuntimeException(
                                                "Question 'Nom du laboratoire d’accueil' was not found.")));

                student.setAnswers(new ArrayList<>(List.of( //
                                situationAnswer,
                                subjectOfYourDoctorateAnswer,
                                hostLaboratoryNameAnswer)));

                answerRepository.save(situationAnswer);
                answerRepository.save(subjectOfYourDoctorateAnswer);
                answerRepository.save(hostLaboratoryNameAnswer);
                studentRepository.save(student);

        }

}