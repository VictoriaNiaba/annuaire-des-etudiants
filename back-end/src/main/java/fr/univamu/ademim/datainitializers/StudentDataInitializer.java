package fr.univamu.ademim.datainitializers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.exception.RoleNotFoundException;
import fr.univamu.ademim.model.Group;
import fr.univamu.ademim.model.Role;
import fr.univamu.ademim.model.Role.RoleName;
import fr.univamu.ademim.model.Section;
import fr.univamu.ademim.model.Student;
import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.repositories.GroupRepository;
import fr.univamu.ademim.repositories.RoleRepository;
import fr.univamu.ademim.repositories.SectionRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(4)
@Component
public class StudentDataInitializer implements DataInitializer {

    private static final String ROLE_NOT_FOUND = "Role %s was not found";
    private static final String DEFAULT_PASSWORD = "pwd";

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private SectionRepository sectionRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void initialize() {
        if (groupRepository.count() > 0) {
            return;
        }

        log.info("Creating default students and groups.");

        Role studentRole = roleRepository.findByName(RoleName.STUDENT)
                .orElseThrow(() -> new RoleNotFoundException(String.format(ROLE_NOT_FOUND, RoleName.STUDENT)));

        UserAccount userAccount = new UserAccount();
        userAccount.addRole(studentRole);
        userAccount.setEmailAddress("lina.caxeiro@gmail.com");
        userAccount.setFirstname("Lina");
        userAccount.setLastname("Caxeiro");
        userAccount.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
        Student student = new Student();
        student.setUserAccount(userAccount);
        student.setLinkedin("www.linkedin.com/in/lina-caxeiro");
        student.setDateOfBirth(LocalDate.of(1997, 4, 13));

        Group group = new Group();
        group.setName("Master Fiabilité et sécurité informatique 2020-2021");

        Section research = sectionRepository.findByTitle("Recherche")
                .orElseThrow(() -> new RuntimeException("Section 'Recherche' does not exists"));
        group.setSections(List.of(research));
        group.setStudents(List.of(student));
        student.setGroup(group);

        groupRepository.save(group);
    }

}