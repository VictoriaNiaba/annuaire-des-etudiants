package fr.univamu.ademim.datainitializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.model.Role;
import fr.univamu.ademim.model.Role.RoleName;
import fr.univamu.ademim.repositories.RoleRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(1)
@Component
public class RoleDataInitializer implements DataInitializer {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void initialize() {
        if (roleRepository.count() > 0) {
            return;
        }

        log.info("Creating default roles.");

        Role adminRole = new Role(RoleName.ADMINISTRATOR);
        roleRepository.save(adminRole);
        Role teacherRole = new Role(RoleName.TEACHER);
        roleRepository.save(teacherRole);
        Role studentRole = new Role(RoleName.STUDENT);
        roleRepository.save(studentRole);
    }

}