package fr.univamu.ademim.eventhandler;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeLinkSave;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import fr.univamu.ademim.exception.RoleNotFoundException;
import fr.univamu.ademim.model.Group;
import fr.univamu.ademim.model.Role;
import fr.univamu.ademim.model.UserAccount;
import fr.univamu.ademim.model.Role.RoleName;
import fr.univamu.ademim.repositories.RoleRepository;

@Component
@RepositoryEventHandler(Group.class)
public class GroupEventHandler {

    private static final String ROLE_NOT_FOUND = "Role %s was not found";

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @HandleBeforeCreate
    public void handleGroupBeforeCreate(Group group) {
        String encodedPassword = passwordEncoder.encode("pwd");
        Role studentRole = roleRepository.findByName(RoleName.STUDENT)
                .orElseThrow(() -> new RoleNotFoundException(String.format(ROLE_NOT_FOUND, RoleName.STUDENT)));
        Set<Role> studentRoles = Set.of(studentRole);

        group.getStudents().forEach(student -> {
            UserAccount userAccount = student.getUserAccount();
            userAccount.setPassword(encodedPassword);
            userAccount.setRoles(studentRoles);
        });
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @HandleBeforeSave
    @HandleBeforeLinkSave
    public void secureSaveOperation(Group group) {
        // Sécurise les modifications de groupes
    }
}